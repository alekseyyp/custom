<?php
namespace Custom\Mail\Service;

use Zend\Mail\Message;
use Zend\Mail\Transport\TransportInterface;
use Zend\View\Model\ViewModel;
use Zend\View\Renderer\RendererInterface;

/**
 * Uj\Mail email service class.
 *
 * @since 1.0
 * @package Uj\Mail\Service
 */
class Email
{
    /**
     * The mail transport
     *
     * @var TransportInterface
     */
    protected $transport = null;

    /**
     * The email renderer.
     *
     * @var RendererInterface
     */
    protected $renderer = null;

    private $layout = "layout/emails";

    private $tries = 0;
    
    private $content;


    const MAX_TRIES = 3;


    /**
     * Initialize the mail service
     *
     * @param TransportInterface $transport
     */
    public function __construct(TransportInterface $transport, RendererInterface $renderer, $layout = null)
    {
        $this->transport = $transport;
        $this->renderer = $renderer;
        if($layout) {

            $this->layout = $layout;

        }
    }

    /**
     * Sends an email.
     *
     * @param string|Message $tpl
     * @param array          $data
     */
    public function send($tpl, array $data = null)
    {
        
        $this->tries = 0;
        if ($tpl instanceof Message) {
            $mail = $tpl;
        } else {
            if ($data === null) {
                throw new \InvalidArgumentException('Expected data to be array, null given.');
            }

            $mail = $this->getMessage($tpl, $data);
        }
        
        return $this->sendEmail($mail);
    }

    private function sendEmail($mail)
    {
        $this->tries++;
        try {

            return $this->getTransport()->send($mail);

        } catch (\Exception $e) {

            if ($this->tries <= self::MAX_TRIES) {

                sleep(1);
                return $this->sendEmail($mail);

            }

        }
    }

    /**
     * @param  string  $tpl
     * @param  array   $data
     * @return Message
     */
    public function getMessage($tpl, array $data)
    {
        $mail = new Message();

        if (isset($data['encoding'])) {
            $mail->setEncoding($data['encoding']);
        }
        if (isset($data['from'])) {
            $mail->setFrom($data['from'], (isset($data['from_name']) ? $data['from_name'] : null));
        }
        if (isset($data['to'])) {
            $mail->setTo($data['to'], (isset($data['to_name']) ? $data['to_name'] : null));
        }
        if (isset($data['cc'])) {
            $mail->setCc($data['cc']);
        }
        if (isset($data['bcc'])) {
            $mail->setBcc($data['bcc']);
        }
        if (isset($data['subject'])) {
            $mail->setSubject($data['subject']);
        }
        if (isset($data['sender'])) {
            $mail->setSender($data['sender']);
        }
        if (isset($data['replyTo'])) {
            $mail->setReplyTo($data['replyTo']);
        }

        $this->content = $this->renderMail($tpl, $data);
        $bodyPart = new \Zend\Mime\Message();

        $bodyMessage = new \Zend\Mime\Part($this->content);
        $bodyMessage->type = 'text/html';
        $bodyMessage->charset = 'utf-8';

        $bodyPart->setParts(array($bodyMessage));

        $mail->setBody($bodyPart);
        $mail->setEncoding('UTF-8');
       /*  $mail->getHeaders()
            ->addHeaderLine('Content-Type', 'text/html; charset=UTF-8')
            ->addHeaderLine('Content-Transfer-Encoding', '8bit'); */

        return $mail;
    }

    /**
     * Returns the mail transport
     *
     * @return \Zend\Mail\Transport\TransportInterface
     */
    public function getTransport()
    {
        return $this->transport;
    }

    /**
     * Sets the transport
     *
     * @param TransportInterface $transport
     */
    public function setTransport(TransportInterface $transport)
    {
        $this->transport = $transport;
    }

    /**
     * @return \Zend\View\Renderer\RendererInterface
     */
    public function getRenderer()
    {
        return $this->renderer;
    }

    /**
     * @param \Zend\View\Renderer\RendererInterface $renderer
     */
    public function setRenderer(RendererInterface $renderer)
    {
        $this->renderer = $renderer;
    }
    
    
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Render a given template with given data assigned.
     *
     * @param  string $tpl
     * @param  array  $data
     * @return string The rendered content.
     */
    protected function renderMail($tpl, array $data)
    {
        $viewModel = new ViewModel($data);
        $viewModel->setTemplate($tpl);

        $layoutModel = new ViewModel(array_merge($data, array(
            'content' => $this->renderer->render($viewModel)
        )));
        $layoutModel->setTemplate( $this->layout);

        return $this->renderer->render($layoutModel);
    }
}
