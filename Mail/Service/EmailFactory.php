<?php

namespace Custom\Mail\Service;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
/**
 * Uj\Mail email service factory.
 *
 * @since 1.0
 * @package Uj\Mail\Service
 */
class EmailFactory implements
    FactoryInterface
{
	/**
	 * The default mail transport implementation namespace.
	 *
	 * @var string
	 */
	const STD_TRANSPORT_NS = 'Zend\Mail\Transport';
	
    /**
     * Create, configure and return the email transport.
     *
     * @see FactoryInterface::createService()
     * @return \Zend\Mail\Transport\TransportInterface
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
    	$config = $serviceLocator->get("config");

    	$transportConfig = $config['mail']['transport'];
        $type = $transportConfig['type'];

        if (false === class_exists($type)) {
            $type = self::STD_TRANSPORT_NS . '\\' . ucfirst($type);
        }

        $transport = new $type;

        if (isset($transportConfig['options'])) {
            // by convention... SmtpOptions, SendmailOptions, etc..
            $optionsClass = $type . 'Options';
            $options = $transportConfig['options'];

            // create instance of options class if conventional
            // try succeeded, otherwise use what's in options
            if (class_exists($optionsClass)) {
                $options = new $optionsClass($options);
            }

            $transport->setOptions($options);
        }
    	if ($serviceLocator->has('viewrenderer')) {
    	    
    	    $renderer  = $serviceLocator->get('viewrenderer');
    	    $layout = null;
    	    
    	} else {

    	    $renderer = new \Zend\View\Renderer\PhpRenderer($config);
    	    $map = new \Zend\View\Resolver\TemplatePathStack();
    	    foreach($config['view_manager']['template_path_stack'] as $path) {
    	        $map->addPath($path);
    	    }

    	    $renderer->setResolver($map);
    	    $renderer->setHelperPluginManager($serviceLocator->get('ViewHelperManager'));
    	    $layout = 'layout/email-layout';
    	    
    	}
       
        return new Email($transport, $renderer, $layout);
    }
}
