<?php
namespace Custom\Logger;

class Logger
{
	private $prefix;

	private $level;

	private $errorToFile;
	private $infoToFile;
	private $debugToFile;

	private $errorRs;
	private $infoRs;
	private $debugRs;

	private $class = "Unknown";

	const ERROR = "error";
	const INFO = "info";
	const DEBUG = "debug";


	public function __construct($level)
	{
		$this->level = strtolower($level);
	}

	public function __destruct()
	{
		if($this->errorRs)
		{
			fclose($this->errorRs);
		}
		if($this->infoRs)
		{
			fclose($this->infoRs);
		}
		if($this->debugRs)
		{
			fclose($this->debugRs);
		}
	}

	public function setClass($class)
	{
		$this->class = $class;
		return $this;
	}

	public function error($error, $scopeObject = null)
	{
		if ($scopeObject) {
			$this->setClass(get_class($scopeObject));
		}
		$this->log($this->buildMessage($error, self::ERROR), $this->getResource(self::ERROR));
		return $this;
	}

	public function info($info, $scopeObject = null)
	{
		if($this->level == self::ERROR)
		{
			return $this;
		}
		if ($scopeObject) {
			$this->setClass(get_class($scopeObject));
		}
		$this->log($this->buildMessage($info, self::INFO), $this->getResource(self::INFO));
		return $this;
	}

	public function debug($debug, $scopeObject = null)
	{
		if($this->level != self::DEBUG)
		{
			return $this;
		}
		if ($scopeObject) {
			$this->setClass(get_class($scopeObject));
		}
		$this->log($this->buildMessage($debug, self::DEBUG), $this->getResource(self::DEBUG));
		return $this;
	}

	private function getResource($level)
	{
		if($level == self::INFO && $this->infoToFile){
			if(!$this->infoRs)
			{
				$this->infoRs = $this->openFile($this->infoToFile);
			}
			return $this->infoRs;
		}
		elseif($level == self::DEBUG && $this->debugToFile)
		{
			if(!$this->debugRs)
			{
				$this->debugRs = $this->openFile($this->debugToFile);
			}
			return $this->debugRs;
		}
		if(!$this->errorRs)
		{
			$this->errorRs = $this->openFile($this->errorToFile);
		}
		return $this->errorRs;
	}

	private function openFile($fileName)
	{
		return fopen($fileName, "a+");
	}

	private function log($msg, $fileResource)
	{
		fwrite($fileResource, $msg);
	}

	private function buildMessage($message, $level)
	{
		return date("d/m/Y H:i:s") . " *".strtoupper($level)."* in ".
						$this->class .": " . $message . "\n";
	}

	public function setErrorFile($errorFile)
	{
		$this->errorToFile = $errorFile;
	}

	public function setInfoFile($infoFile)
	{
		$this->infoToFile = $infoFile;
	}

	public function setDebugFile($debugFile)
	{
		$this->debugToFile = $debugFile;
	}
}