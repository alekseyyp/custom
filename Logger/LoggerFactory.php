<?php
namespace Custom\Logger;


class LoggerFactory
{
	private static $defaultErrorLogFile = "error.log";
	private static $levels = array(
		"DEBUG", // Log everything
		"INFO", // Log INFO and ERROR
		"ERROR" // Log just ERROR - this is the best option for live
	);
	
	public static function create($logsFolder, $level = "INFO", $errorLog = null, $infoLog = null, $debugLog = null)
	{
		if(!in_array($level, self::$levels))
		{
			throw new \Exception("Wrong Log's level");
		}
		$logsFolder = rtrim($logsFolder, "/") . "/";
		
		if(file_exists($logsFolder) == false)
		{
			throw new \Exception("Folder " . $logsFolder . " does not exists");
		}
		
		if($errorLog == null)
		{
			$errorLog = self::$defaultErrorLogFile;
		}
		
		$logger = new Logger($level);
		
		if($errorLog)
		{
			/* if(file_exists($logsFolder . $errorLog) == false)
			{
			} */
			$logger->setErrorFile($logsFolder . $errorLog);
		}
		
		if($infoLog)
		{
			/* if(file_exists($logsFolder . $infoLog) == false)
			{
			} */
			$logger->setInfoFile($logsFolder . $infoLog);
		}

		if($debugLog)
		{
			/* if(file_exists($logsFolder . $debugLog) == false)
			{
			} */
			$logger->setDebugFile($logsFolder . $debugLog);
		}
		
		return $logger;
	}
}