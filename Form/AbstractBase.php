<?php
namespace Custom\Form;
use Zend\Form\Form;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;

abstract class AbstractBase extends Form{

    const DEFAULT_BEHAVIOUR = 'default';

    abstract protected function elements();
    abstract protected function filters();

    protected $_behaviours = array();
    protected $behaviour;

    private $filters = array();
    private $_inputFactory;

    private $elementsPrepared = false;

    public function __construct($behaviour = self::DEFAULT_BEHAVIOUR)
    {
        $this->behaviour = $behaviour;
        parent::__construct($behaviour);
    }

    public function prepare()
    {
        parent::prepare();
    }

    public function prepareForm()
    {

        if ($this->elementsPrepared == false) {

            $this->elements();
            $this->filters();
            $this->setInputFilter($this->getFilters());
            $this->elementsPrepared = true;
        }
    }

    public function getFilters()
    {
        if (isset($this->filters[$this->behaviour])) {

            return $this->filters[$this->behaviour];

        }

        return null;
    }

    /**
     * This function adds a new filter to the collection
     * @param array $config
     * @return \Custom\Form\AbstractBase
     */
    protected function addFilter(array $config, $behaviour = null)
    {
        if ($behaviour == null) {

            $behaviour = $this->behaviour;

        }
        if (!isset($this->filters[$behaviour])) {

            $this->filters[$behaviour] = new InputFilter();

        }
        if (!$this->_inputFactory) {

            $this->_inputFactory = new InputFactory();

        }


        $this->filters[$behaviour]->add($this->_inputFactory->createInput($config));
        return $this;
    }
}