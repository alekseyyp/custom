<?php
namespace Custom\Router;
use Zend\Mvc\Router\Http\Segment;
use Zend\Stdlib\RequestInterface as Request;
use Zend\Mvc\Router\RouteMatch;
//use Zend\Mvc\Router\Http\RouteInterface;
//use Zend\Stdlib\RequestInterface as Request;
//use Zend\Stdlib\ArrayUtils;
//use Zend\Mvc\Router\Exception\InvalidArgumentException;
class Standard extends Segment
{
	public function match(Request $request, $pathOffset = null)
	{
		if (!method_exists($request, 'getUri')) {
			return null;
		}
		
		$uri  = $request->getUri();
		$path = $uri->getPath();
		
		
		$module_name = null;
		if(isset($this->parts[0]) && count($this->parts[0]) == 2 && $this->parts[0][0] == 'literal'){
			$module_name = ltrim($this->parts[0][1], '/');
		}
	
		
		if ($pathOffset !== null) {
			$result = preg_match('(\G' . $this->regex . ')', $path, $matches, null, $pathOffset);
		} else {
			$result = preg_match('(^' . $this->regex . '$)', $path, $matches);
		}
		//echo '<br>'.$path.'<br>';
		//var_dump($this->regex);
	
		if (!$result) {
			return null;
		}
	
		$matchedLength = strlen($matches[0]);
		$params        = array();
		
		foreach ($this->paramMap as $index => $name) {
			if (isset($matches[$index]) && $matches[$index] !== '') {
				if($module_name && $name == 'controller' && !preg_match('/Controller/', $matches[$index])){
					$matches[$index] = $module_name.'\Controller\\'.ucfirst($matches[$index]);
				}
				$params[$name] = $this->decode($matches[$index]);
			}
		}
		return new RouteMatch(array_merge($this->defaults, $params), $matchedLength);
	}
}