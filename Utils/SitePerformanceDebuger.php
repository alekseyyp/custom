<?php
namespace Custom\Utils;
/* *
* @author Aleksey Prozhoga
 */
class SitePerformanceDebugger {
	private static $instance = null;
	private $info = array();
	private $startTime = null;
	private $lastTime  = null;
	private function __construct(){
		$this->lastTime = $this->startTime = microtime(true);
	}
	
	public static function getInstance(){
		if(!self::$instance){
			self::$instance = new SitePerformanceDebugger();
		}
		return self::$instance;
	}
	
	public function timepoint($key){
		$mt = microtime(true);
		if(isset($this->info[$key])){
			$key .= rand(0, 100);
		}
		$this->info[$key] = "From p.point ".round($mt - $this->lastTime, 3)."s. Total ".round($mt - $this->startTime, 3)."s";
		$this->lastTime = $mt;
	}
	
	public function printInfo(){
		$this->timepoint("End of this page");
		echo '<table border="1">';
		foreach($this->info as $k=>$v){
			echo '<tr><td>'.$k.'</td><td>'.$v.'</td></tr>';
		}
		echo '</table>';
	}
}