<?php
namespace Custom\ZfcUser;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;


class UserMapperFactory implements FactoryInterface{
	public function createService(ServiceLocatorInterface $serviceLocator)
	{
		return $serviceLocator->get("App\Mapper\User");
	}
}