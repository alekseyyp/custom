<?php
namespace Custom\Search;

use ZendSearch\Lucene\Lucene,
	ZendSearch\Lucene\Document,
	ZendSearch\Lucene\Document\Field,
	ZendSearch\Lucene\Analysis\Analyzer\Common\Utf8\CaseInsensitive,
	ZendSearch\Lucene\Analysis\Analyzer\Analyzer,
	ZendSearch\Lucene\Search\Query\MultiTerm,
	ZendSearch\Lucene\Index\Term,
	ZendSearch\Lucene\Search\QueryParser;

class SearchLucene {
	private $path;
	private $index;
	private $total;
	private static $instance;
	
	private function __construct(){
		$this->path = getcwd().'/data/pages-index';
		//setlocale(LC_ALL, 'ru_RU.utf-8');
		$analyzer = new CaseInsensitive();
		Analyzer::setDefault($analyzer);
		
		if(!file_exists($this->path)){
			$this->index = Lucene::create($this->path);
		}else{
			$this->index = Lucene::open($this->path);
		}
	}
	
	public function addToIndex(\App\Search\Lucene\OptionsContainer $options){
		$doc = new Document();
		$list = $options->getAll();
		foreach($list as &$option){
			$type = $option->type;
			$doc->addField(Field::$type($option->name, $option->value, 'utf-8'));
		}
		$this->index->addDocument($doc);
		$this->index->commit();
	}
	
	public function removeFromIndex($conditions){

		$query = new MultiTerm();
		 
		foreach($conditions as $key=> &$value){
			$query->addTerm(new Term($value, $key), true);
		}
		 
		$hits = $this->index->find($query);
		$len = count($hits);
		if($len > 0){
			foreach ($hits as &$hit){
			  $this->index->delete($hit->id);
			}
		}
		return $len;
	}
	
	public function search($query, array $params, $limit = 10, $sort = null){
		//Lucene::setResultSetLimit($limit);
		QueryParser::setDefaultEncoding('utf-8');
		$userQuery = QueryParser::parse($query);
		if($sort){
			$hits = $this->index->find($userQuery, $sort['field'], $sort['type'], $sort['order']);
		}else{
			$hits = $this->index->find($userQuery);
		}
		$list = array();
		$this->total = count($hits);
		if($this->total){
			$i = 0;
			foreach($hits as &$hit){
				$doc = $hit->getDocument();
				$tmp = array();
				foreach($params as $v){
					$tmp[$v] = $doc->getFieldValue($v);
				}
				$tmp['score'] = $hit->score;
				$list[] = $tmp;
				$i++;
				if($limit == $i) break;
				
			}
		}
		return $list;
	}
	
	public function cleanValue($query){
		return str_replace(array("+", "-", "&", "|", "!", "(", ")", "{", "}", "[", "]", "^", "\"", "~", "*", "?", ":", "\\"), "", $query);
	}
	
	public function getTotal(){
		return $this->total;
	}

	
	public function getEngine(){
		return $this->index;
	}
	
	public static function getIndex(){
		if(!self::$instance){
			self::$instance = new self();
		}
		return self::$instance;
	}
	
}