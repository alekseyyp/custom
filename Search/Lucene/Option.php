<?php
namespace Custom\Search\Lucene;

class Option {
	public $type;
	public $name;
	public $value;
	
	public function __construct($type, $name, $value){
		if(!in_array($type, array("Keyword", "UnIndexed", "Binary", "Text", "UnStored"))){
			throw new Exception("Wrong option type");
		}
		if(!is_string($name)){
			throw new Exception("Option name should be a string.");
		}
		$this->type		= $type;
		$this->name		= $name;
		$this->value	= $value; 
	}
}