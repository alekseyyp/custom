<?php
namespace Custom\Search\Lucene;

use Custom\Search\Lucene\Option;

class OptionsContainer {
	private $options;
	public function __construct(){
		
	}
	
	public function add($config){
		if(!$config instanceof Option){
			$option = new Option($config['type'], $config['name'], $config['value']);
		}else{
			$option = $config;
		}
		$this->options[] = $option;
	}
	
	public function getAll(){
		return $this->options;
	}
}