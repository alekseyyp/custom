<?php
namespace Custom\Controller\Plugin;
use Zend\Mvc\Controller\Plugin\AbstractPlugin,
	Zend\ServiceManager\ServiceLocatorAwareInterface,
	Zend\ServiceManager\ServiceLocatorInterface;
class Logger extends AbstractPlugin implements ServiceLocatorAwareInterface{
	
	protected $pluginManager;
	
	private $logger;
	
	public function __invoke($class = null)
    {
    	if(!$this->logger)
    	{
    		$this->logger = $this->getServiceLocator()->getServiceLocator()->get("Logger");
    	}
        if ($class != null) {
        	$this->logger->setClass($class);
        }

        return $this->logger;
    }
	
	public function setServiceLocator(ServiceLocatorInterface $pluginManager)
	{
		$this->pluginManager = $pluginManager;
	}
	
	public function getServiceLocator(){
		return $this->pluginManager;
	}
}