<?php
/* *
* @author Aleksey Prozhoga
 */
namespace Custom\Controller\Plugin;
use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use Zend\Debug\Debug as D;
class Script extends AbstractPlugin{
	public function addJs($filename){
		$this->getController()
				->getServiceLocator()
				->get('viewhelpermanager')
				->get('headScript')
				->appendFile($filename);
		return $this;
	}
	
	public function addCss($filename){
		$this->getController()
				->getServiceLocator()
				->get('viewhelpermanager')
				->get('headLink')
				->appendStylesheet($filename);
		return $this;
	}
	/**
	 * This function adds title to the layout, append to the headTitle
	 * @param String $title
	 * @return Object $this;
	 */
	public function addTitle($title){
		$this->getController()
				->getServiceLocator()
				->get('viewhelpermanager')
				->get('headTitle')
				->append($title);
		$this->getController()->layout()->title = $title;
		return $this;
	}
	
	/**
	 * This function adds meta to the layout, append to the headMeta
	 * @param String $tag;
	 * @param String $value;
	 * @return Object $this;
	 */
	public function addMeta($tag, $value){
		
		$this->getController()
				->getServiceLocator()
				->get('viewhelpermanager')
				->get('headMeta')
				->appendName($tag, $value);
		$this->getController()->layout()->$tag = $value;
		return $this;
	}

}