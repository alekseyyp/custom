<?php
/* *
* @author Aleksey Prozhoga
 */
namespace Custom\Controller\Plugin;
use Zend\Mvc\Controller\Plugin\AbstractPlugin;

class Response extends AbstractPlugin{
	
public function __invoke($data = null, $json = true)
    {
        if (null !== $data) {
        	if($json == true && !isset($data['success'])){
				$data['success'] = true;
			}
            return $this->send($data, $json);
        }

        return $this;
    }
	
	public function send($data, $json = true){
		$response = $this->getController()->getResponse();
		$response->setStatusCode(200);
		if($json == true){
			$response->setContent(\Zend\Json\Json::encode($data));		
		}else{
			$response->setContent($data);
		}
		return $response;
	}
}