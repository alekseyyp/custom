<?php

namespace App\Controller\Plugin;
use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use Zend\Debug\Debug as D;
class Debug extends AbstractPlugin{
	public function log($data, $filename = "main"){
		D::setSapi("cli");
        $msg = D::dump($data, date("Y-m-d H:i:s"), false);
        $fr = fopen(getcwd()."/data/".$filename.".log", "a+");
		fwrite($fr, $msg, strlen($msg));
		fclose($fr);
	}
}