<?php
/* *
* @author Aleksey Prozhoga
 */
namespace Custom\Controller\Plugin;
use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use Zend\View\Model\ViewModel;

class Htmlresponse extends AbstractPlugin{
	
	public function __invoke(array $data, $template, $variables = array(), $title = "")
    {
        if (null !== $data) {
            return $this->send($data, $template, $variables, $title);
        }

        return $this;
    }
	
	public function send(array $data, $template, $variables = array(), $title = "")
	{
		$htmlViewPart = new ViewModel();
		$htmlViewPart->setTerminal(true)
		->setTemplate($template)
		->setVariables($variables);
		
		$sl = $this->getController()->getServiceLocator();
		
		$htmlOutput = $sl
		->get('viewrenderer')
		->render($htmlViewPart);
			
		$translator = $sl->get("translator");
		$data['body'] 	= $htmlOutput;
		$data['title']	= $translator->translate($title);
		
		$response = $this->getController()->getResponse();
		$response->setStatusCode(200);
		$response->setContent(\Zend\Json\Json::encode($data));
		return $response;
	}
}