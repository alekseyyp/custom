<?php
/* *
* @author Aleksey Prozhoga
 */
namespace Custom\Controller\Plugin\Grid;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Like;
use Zend\Db\Sql\Predicate\Predicate;

use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;


use Zend\Debug\Debug as D;

class Grid extends AbstractPlugin{
	private $sql;
	private $select;
	private $adpter;


	protected $mLimit 	= 10;
	protected $mPage	= 0;
	protected $mStart 	= 0;
	protected $dataSource = null;
	protected $defaultField = null;
	protected $mSearch = null;
	protected $_orderBy = null;//
	protected $_orderDir = null;//
	protected $mShift = 0;
	protected $searchColumns = array();
	protected $rowCount;


	/**
	 * @return void
	 */
	public function __construct(){}


	/**
	 */
	public function setSource(Select $select)
	{
		$this->select = $select;
		return $this;
	}

	public function setAdapter(\Zend\Db\Adapter\Adapter $adapter)
	{
		$this->adapter = $adapter;
		return $this;
	}

	/**
	 */
	public function getData()
	{
		 if ($this->mSearch){
			$orWhere = new Predicate(null, Predicate::COMBINED_BY_OR);
			foreach($this->searchColumns as $col){
				if (is_array($col)){
					//TODO smart where add
				}else if(is_string($col)){
					$orWhere->like($col, '%'.$this->mSearch.'%');
				}
			}

			$this->select->where($orWhere, PredicateSet::COMBINED_BY_AND);
		}

		if ($this->_orderBy != null){
			$this->select->order(array($this->_orderBy => $this->_orderDir));
		}

		$data = array();

		$pagination = new Paginator(new DbSelect($this->select, $this->adapter, null));

		$pagination->setItemCountPerPage($this->mLimit);
		$pagination->setCurrentPageNumber($this->mPage);


		$data['total'] = $pagination->getTotalItemCount();

		if ($data['total'] > 0){

			$list = array();
			foreach ($pagination as $row) {
				$list[] = $row;
			}

			$data['data'] = $list;
		}else{
			$data['data'] = array();
		}
		return $data;
	}



	public function initGrid($select, \Zend\Db\Adapter\Adapter $adapter, $params)
	{


		$this->setSource($select);
		$this->setAdapter($adapter);

		$start = isset($params['start'])?$params['start']:null;
		if (isset($start)) {

			$this->mStart = intval($start);

		}
		$page = isset($params['page'])?$params['page']: 1;
		if (isset($page)) {

			$this->mPage = intval($page);

		}
		if (intval($this->mStart) <= 0) {

			$this->mStart = 0;

		}

		$limit = isset($params['limit']) ? $params['limit'] : null;
		if (!empty($limit)) {

			$this->mLimit = intval($limit);

		}


		$search = isset($params['query']) ? $params['query'] : null;

		if (!empty($search)) {

			$this->mSearch = $search;

		}

		$sort = isset($params['sort']) ? $params['sort'] : null;

		if (!empty($sort)) {

			$this->_orderBy = $sort;
			$dir = isset($params['dir']) ? $params['dir'] : null;
			if (!empty($dir) && ($dir == 'DESC')) {

				$this->_orderDir = 'DESC';

			} else {

				$this->_orderDir = 'ASC';

			}
		}

		return $this;
	}

	public function setSearchColumns($columns)
	{
		$this->searchColumns = $columns;
		return $this;
	}

}