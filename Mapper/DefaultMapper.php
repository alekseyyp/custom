<?php
namespace Custom\Mapper;

use Zend\Db\Sql\Expression;
use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Stdlib\Hydrator\Filter\FilterComposite;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface,
	Zend\EventManager\EventManager,
	Zend\ServiceManager\ServiceLocatorAwareInterface,
	Zend\ServiceManager\ServiceLocatorInterface;


class DefaultMapper implements InputFilterAwareInterface, ServiceLocatorAwareInterface{

	protected $tableGateway;

	protected $inputFilter;

	protected $serviceLocator;

	protected $events;

	protected $filterFields = array();

	public function __construct(TableGateway $tableGateway)
	{
		$this->tableGateway = $tableGateway;
	}

/* 	public function getDataGridSelect(){
		$select = $this->tableGateway->getSql()
		->select()
		->columns(array("*"));

		return $select;
	} */

    public function count($where) {
        $select = $this->tableGateway->getSql()
            ->select()
            ->where($where)
            ->columns(array("c" => new Expression("COUNT(*)")));

        $result = $this->tableGateway->getSql()->prepareStatementForSqlObject($select)->execute();
        $row = $result->current();
        return ($row ? $row['c'] : 0);
    }


    /**
     * Search by $where statement. Uses Pagination.
     *
     * @param null|\Zend\Db\Sql\Where $where
     * @param null|Zend\Db\Sql\Select $select
     * @param null|boolean $usePrototype
     * @param null|int $pageRange
     * @return Paginator
     */
	public function search($where = null, $select = null, $usePrototype = true, $pageRange = 4)
    {
        if (!$select) {
            $select = $this->tableGateway->getSql()->select();
        }
        if($where != null){
            $select->where($where);
        }

        $pagination = new Paginator(new DbSelect(
        								$select,
        								$this->tableGateway->getAdapter(),
        								($usePrototype == true ? $this->tableGateway->getResultSetPrototype() : null)
        							));
        $pagination->setPageRange($pageRange);
        return $pagination;
    }

	public function fetchAll($where = null, $order = null, $limit = null)
	{
		$select = $this->tableGateway->getSql()->select();
		if($where != null){
			$select->where($where);
		}
		if($order != null){
			$select->order($order);
		}
		if($limit != null){
			$select->offset(0);
			$select->limit($limit);
		}
		//echo $select->getSqlString(); exit;
		$resultSet = $this->tableGateway->selectWith($select);
		$resultSet->buffer();
		return $resultSet;
	}

	public function fetchOne($id)
	{
		$id  = (int) $id;
		$rowset = $this->tableGateway->select(array('id' => $id));
		return  $rowset->current();
	}

	public function fetchOneByAttribute($attribute, $value){
		$rowset = $this->tableGateway->select(array($attribute => $value));
		return  $rowset->current();
	}

	public function fetchOneByAttributes($attributes){
		$rowset = $this->tableGateway->select($attributes);
		return  $rowset->current();
	}

	public function save(& $model, $attributes = null)
	{

		$this->events()->trigger(__FUNCTION__ . '.pre', $this, $model);

		$data = $this->extract($model);
		unset($data['id']);

		if($attributes)
		{
			$newData = array();
			foreach($attributes as $v)
			{
				if(array_key_exists($v, $data))
				{
					$newData[$v] = $data[$v];
				}
			}
			$data = $newData;
		}

		$exists = false;
		if (method_exists($model, 'getId')) {

			$exists = true;
		}
		if ($exists == false || ($id = (int)$model->getId()) == 0) {

			$this->tableGateway->insert($data);
			if ($exists == true) {

				$model->setId($this->tableGateway->lastInsertValue);

			}

		} else {
			$this->tableGateway->update($data, array('id' => $id));
		}
		$this->events()->trigger(__FUNCTION__ . '.post', $this, $model);
	}

	public function extract($model){
		$hydrator = $this->tableGateway->getResultSetPrototype()->getHydrator();
		$mapper = $this;
		$hydrator->addFilter("filterFields",
				function($property) use ($mapper){
			if(in_array($property, $mapper->getFilterFields())){
				return false;
			}
		}, FilterComposite::CONDITION_AND
		);
		return $hydrator->extract($model);
	}

	public function hydrate(array $data, $model){
		$hydrator = $this->tableGateway->getResultSetPrototype()->getHydrator();
		$mapper = $this;
		$hydrator->addFilter("filterFields",
				function($property) use($mapper){
			if(in_array($property, $mapper->getFilterFields())){
				return false;
			}
		}, FilterComposite::CONDITION_AND
		);
		return $hydrator->hydrate($data, $model);
	}

	public function setInputFilter(InputFilterInterface $inputFilter)
	{
		throw new \Exception("Not used");
	}

	public function getInputFilter()
	{
		throw new \Exception('Not used');
		return $this->inputFilter;
	}

	public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
	{
		$this->serviceLocator = $serviceLocator;
	}

	public function getServiceLocator(){
		return $this->serviceLocator;
	}

	public function setEventManager(EventManager $events)
	{
		$this->events = $events;
	}

	public function events()
	{
		if (!$this->events) {
			$this->setEventManager(new EventManager(
					array(__CLASS__, get_called_class())
			));
		}
		return $this->events;
	}

	public function delete($id)
	{
		if($id > 0){
			$this->tableGateway->delete(array("id" => $id));
			return true;
		}
	}

	public function getFilterFields()
	{
		return $this->filterFields;
	}

	public function printSql(\Zend\Db\Sql\Select $select, $return = 0, $exit = 1)
	{
		$sql = $select->getSqlString($this->tableGateway->getSql()->getAdapter()->getPlatform());
		if($return == 1)
		{
			return $sql;
		}
		else
		{
			echo $sql;
		}
		if($exit == 1)
		{
			exit;
		}
	}
}

?>
