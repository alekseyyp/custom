<?php
namespace Custom\View\Helper;

use Zend\View\Helper\AbstractHelper,
	Zend\ServiceManager\ServiceLocatorAwareInterface,
	Zend\ServiceManager\ServiceLocatorInterface;


class Logger extends AbstractHelper implements ServiceLocatorAwareInterface
{
	protected $pluginManager;
	
	private $logger;
	
	public function __invoke($template = null)
    {
    	if(!$this->logger)
    	{
    		$this->logger = $this->getServiceLocator()->getServiceLocator()->get("Logger");
    	}
        if ($template != null) {
        	$this->logger->setClass("Template - " . $template);
        }

        return $this->logger;
    }

	public function setServiceLocator(ServiceLocatorInterface $pluginManager)
	{
		$this->pluginManager = $pluginManager;
	}
	
	public function getServiceLocator(){
		return $this->pluginManager;
	}
}