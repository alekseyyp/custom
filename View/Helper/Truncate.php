<?php
namespace Custom\View\Helper;

use Zend\View\Helper\AbstractHelper;

class Truncate extends AbstractHelper 
{
	public function __invoke($string, $length = 50, $postfix = '...')
	{
		$truncated = trim($string);
        $length = (int)$length;
        if (!$string) {
            return $truncated;
        }
        $fullLength = iconv_strlen($truncated, 'UTF-8');
        if ($fullLength > $length) {
            $truncated = trim(iconv_substr($truncated, 0, $length, 'UTF-8')) . $postfix;
        }
        return $truncated;
	}
}