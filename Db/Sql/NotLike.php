<?php
namespace Custom\Db\Sql;
use Zend\Db\Sql\Predicate\Like;

class NotLike extends Like
{

    protected $specification = '%1$s NOT LIKE %2$s';

}