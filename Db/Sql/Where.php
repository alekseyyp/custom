<?php
namespace Custom\Db\Sql;

use Zend\Db\Sql\Where as SqlWhere;

class Where extends SqlWhere
{
	public function notLike( $identifier, $like ) {
	    $this->addPredicate( new NotLike( $identifier, $like ), ($this->nextPredicateCombineOperator) ? : $this->defaultCombination  );
	    $this->nextPredicateCombineOperator = null;
	    return $this;
	}
}