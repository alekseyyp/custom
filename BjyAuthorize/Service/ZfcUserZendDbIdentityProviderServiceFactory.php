<?php
namespace Custom\BjyAuthorize\Service;

use BjyAuthorize\Provider\Identity\ZfcUserZendDb;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ZfcUserZendDbIdentityProviderServiceFactory implements FactoryInterface
{
    /**
     * {@inheritDoc}
     *
     * @return \BjyAuthorize\Provider\Identity\ZfcUserZendDb
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        /* @var $adapter \Zend\Db\Adapter\Adapter */
        $adapter     = $serviceLocator->get('zfcuser_zend_db_adapter');
        /* @var $userService \ZfcUser\Service\User */
        $userService = $serviceLocator->get('Custom\ZfcUser\Service\User');
        $config      = $serviceLocator->get('BjyAuthorize\Config');

        $provider = new ZfcUserZendDb($adapter, $userService);

        $provider->setDefaultRole($config['default_role']);

        return $provider;
    }
}
