<?php

namespace Custom\Navigation\Service;

use Zend\Navigation\Service\AbstractNavigationFactory;
/**
 * Custom navigation factory.
 */
class AuthorNavigationFactory extends AbstractNavigationFactory
{
    /**
     * @return string
     */
    protected function getName()
    {
        return 'author';
    }
}
